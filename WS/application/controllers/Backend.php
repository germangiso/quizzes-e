<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->library('ion_auth');

         if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } 

		$this->load->library('grocery_CRUD');
	}

	public function _output_this($output = null)
	{
            $this->load->model('channel_model');
            $channel = $this->channel_model->get_channel_names();
            foreach($channel as $option)$output->channels[$option->id]=$option->name;
            $output->channelidSelected=$this->session->userdata("channelid");
		    $this->load->view('backend.php',$output);
	}

	public function setChannel($channelid)
	{
       $this->session->set_userdata('channelid', $channelid);
	   redirect('/backend/quizzes_management', 'refresh');
	}

	public function index()
	{
		$this->_output_this((object)array('channelid' => '' ,'output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function quizzes_management()
	{
			$crud = new grocery_CRUD();
			$crud->set_table('quiz');
            $crud->set_theme('datatables'); 
            $crud->set_relation_n_n('Questions', 'quiz_question', 'question', 'quiz_id', 'question_id','question', 'priority', array('channel_id'=>$this->session->userdata("channelid")));
            $crud->set_relation('channel_id','channel','name');
            $crud->set_relation_n_n('Results', 'quiz_result', 'result', 'quiz_id', 'result_id','result', 'priority', array('channel_id'=>$this->session->userdata("channelid")));

            $crud->columns('id','quiz','channel_id','active','Questions');
            $crud->fields('quiz','channel_id','active','Questions','imageurl','imageurlshort','Results');
            $crud->where('channel_id',$this->session->userdata("channelid"));
			$crud->set_subject('Quiz');
            $crud->callback_add_field('channel_id', array($this, 'optionsChannels'));
			$crud->display_as('channel_id','Canal')
				 ->display_as('Questions','Preguntas')
				 ->display_as('imageurl','Imagen Akamai')
                 ->display_as('imageurlshort','Imagen Akamai Mobile')
				 ->display_as('active','Activo')
				 ->display_as('Results','Resultados');
           
			$output = $crud->render();
            $output->section="quizzes";
			$this->_output_this($output);

            /*$crud = new grocery_CRUD();
            $crud->set_table('quiz');
            $crud->set_theme('datatables'); 

            $crud->set_relation_n_n('Questions', 'quiz_question', 'question', 'quiz_id', 'question_id','question', 'priority', array('channel_id'=>$this->session->userdata("channelid")));
            $crud->set_relation('channel_id','channel','name');
            $crud->set_relation_n_n('Results', 'quiz_result', 'result', 'quiz_id', 'result_id','result', 'priority', array('channel_id'=>$this->session->userdata("channelid")));

            $crud->columns('id','quiz','channel_id','active','Questions');
            $crud->fields('quiz','channel_id','active','Questions','imageurl','Results', 'titulochoque', 'subtitulochoque', 'logoshow');
            $crud->where('channel_id',$this->session->userdata("channelid"));
            $crud->set_subject('Channel');

            $output = $crud->render();
            $output->section="channels";
            $this->_output_this($output);*/
    

	}

    public function optionsChannels($value = null)
    {
            $this->load->model('channel_model');
            $channel = $this->channel_model->get_channel_names();
            $channelsOp="<select name='channel_id'>";
            foreach($channel as $option){
                if($this->session->userdata("channelid")==$option->id)
                    $selected="SELECTED";
                else
                    $selected="";

                $channelsOp.="<option value='".$option->id."' $selected>".$option->name."</option>";
            }
            $channelsOp.="</select>";
            return $channelsOp;
    }

    public function optionsResults($value = null)
    {
        $crud = new grocery_CRUD();
        $state_info = $crud->getStateInfo();
        $primary_key = $state_info->primary_key;

        $query = $this->db->query("SELECT result_id FROM answer WHERE id=$primary_key LIMIT 1");
        $row = $query->row();
        $resultid=$row->result_id;


        $this->load->model('result_model');
        $result = $this->result_model->get_result_names($this->session->userdata("channelid"));
        $resultsOp="<select name='result_id'>";
        foreach($result as $option){

            if($resultid==$option->id)
                $selected="SELECTED";
            else
                $selected="";

            $resultsOp.="<option value='".$option->id."' $selected>".substr($option->texto, 0,69)."...</option>";
        }
        $resultsOp.="</select>";
        return $resultsOp;
    }

	public function questions_management()
	{
			$crud = new grocery_CRUD();
			$crud->set_table('question'); 
            $crud->set_theme('datatables'); 
            $crud->set_relation_n_n('answers', 'question_answer', 'answer', 'question_id', 'answer_id','answer', 'priority', array('channel_id'=>$this->session->userdata("channelid")));
            $crud->set_relation('channel_id','channel','name');
            $crud->where('channel_id',$this->session->userdata("channelid"));

            $crud->columns('id','question','channel_id','answers');
            $crud->fields('question','channel_id','answers');
			$crud->set_subject('Questions');

   			$crud->display_as('channel_id','Canal')->display_as('question','Pregunta')->display_as('answers','Respuestas')->display_as('questions','Preguntas');
            $crud->callback_add_field('channel_id', array($this, 'optionsChannels'));

			$output = $crud->render();
            $output->section="questions";
			$this->_output_this($output);
	}

	public function answers_management()
	{
			$crud = new grocery_CRUD();
			$crud->set_table('answer');
            $crud->set_theme('datatables');
            $crud->set_relation('channel_id','channel','name');
            $crud->set_relation('result_id','result','result');
            $crud->where('answer.channel_id',$this->session->userdata("channelid"));

			$crud->columns('id','channel_id','answer','imageurl');
            $crud->fields('channel_id','answer','imageurl','result_id');
			$crud->set_subject('Opciones');

            $crud->callback_add_field('channel_id', array($this, 'optionsChannels'));
            //$crud->callback_add_field('result_id', array($this, 'optionsResults'));
            $crud->callback_edit_field('result_id', array($this, 'optionsResults'));

   			$crud->display_as('channel_id','Canal')->display_as('answer','Opción')->display_as('imageurl','Imagen Akamai')->display_as('result_id','Resultado Asociado');

			$output = $crud->render();
            $output->section="answers";
			$this->_output_this($output);
	}

	public function channel_management()
	{
			$crud = new grocery_CRUD();
			$crud->set_table('channel');
            $crud->set_theme('datatables'); 
			$crud->columns('id','name');
			$crud->set_subject('Channel');

			$output = $crud->render();
            $output->section="channels";
			$this->_output_this($output);
	}

	public function result_management()
	{
			$crud = new grocery_CRUD();
			$crud->set_table('result');
            $crud->set_theme('datatables'); 

            $crud->columns('id','texto','result','imageurl','channel_id', 'votos');
            $crud->fields('texto','result','imageurl','channel_id');
			$crud->unset_texteditor('texto','full_text');
			$crud->unset_texteditor('result','full_text');

            $crud->set_relation('channel_id','channel','name');
			$crud->set_subject('Resultados');
   			$crud->display_as('result', 'Resultado')->display_as('text', 'Texto resultado')->display_as('imageurl', 'Imagen Akamai')->display_as('channel_id', 'Canal');
            $crud->callback_add_field('channel_id', array($this, 'optionsChannels'));
            $crud->where('channel_id',$this->session->userdata("channelid"));

			$output = $crud->render();
            $output->section="resultados";
			$this->_output_this($output);
	}

}
