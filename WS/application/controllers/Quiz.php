<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Quiz extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html

       This function return all quizzes, it can recieve channel id and return its results

	 */
      /*http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/?format=json&channel=2
        http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/?channel=2
        */
	  public function index_get()
      {
        $this->db->select('quiz.id, quiz.quiz, quiz.imageurl, quiz.imageurlshort'); //, question.question
        $this->db->from('quiz');
        $this->db->join('quiz_question', 'quiz.id = quiz_question.quiz_id');
        $this->db->join('question', 'question.id = quiz_question.question_id');
        $this->db->distinct('quiz');
        if($this->get('channel')!="")$this->db->where("quiz.channel_id=".$this->get('channel'));
        $query = $this->db->get()->result();

        $this->response($query);
      }

      /*http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/getquizzes.xml?channel=1
        http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/getquizzes/?format=json&channel=1
      */
      public function getquizzes_get()
      {
        $this->db->select('id, quiz, imageurl');
        $this->db->from('quiz');
        $this->db->where('channel_id', $this->get('channel'));
        $query = $this->db->get()->result();

        $this->response($query);
      }

      /*http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/getquestions.xml?quizid=2*/
      public function getquestions_get()
      {
        $this->db->select('question_id, question');
        $this->db->from('question');
        $this->db->join('quiz_question', 'question.id = quiz_question.question_id');
        $this->db->where('quiz_question.quiz_id', $this->get('quizid'));
        $this->db->order_by('quiz_question.priority');
        $query = $this->db->get()->result();

        $this->response($query);
      }

      /*http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/getanswers.xml?questionid=2*/
      public function getanswers_get()
      {
        $this->db->select('answer_id, answer, imageurl');
        $this->db->from('answer');
        $this->db->join('question_answer', 'answer.id = question_answer.answer_id');
        $this->db->where('question_answer.question_id', $this->get('questionid'));
		$this->db->order_by('id', 'RANDOM');
        $query = $this->db->get()->result();
		    //$data['data'] = $query;
		    //$this->load->view('welcome_message',$data);

        $this->response($query);
      }

      /*http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/getuserresult/?format=json*/
      public function getuserresult_get()
      {
          $output["persona"]=$this->input->get_post('answered', TRUE);
          $this->load->view('result',$output);
          /* $answered = "aca";
           $this->response($answered);
           $suma=0;
         //$answered="1-2,2-3,4-10";
           $vecans=explode(",",$answered);
           for($i=0;$i<count($vecans);$i++):
             $vec=explode("-",$vecans[$i]);
             $this->db->select('priority');
             $this->db->from('question_answer');
             $this->db->where('question_id', $vec[0]);
             $this->db->where('answer_id', $vec[1]);
             $query = $this->db->get()->result();
             $suma+=$query[0]->priority;
             //$this->load->view('welcome_message',$data);
           endfor;
           $priority=round($suma/$i);

         $this->db->select('result');
         $this->db->from('quiz_result');
         $this->db->join('result', 'result.id = quiz_result.result_id');
         $this->db->where("priority >=$priority AND priority <=$priority");
         $query = $this->db->get()->result();
         $this->response($query);*/
      }

}
