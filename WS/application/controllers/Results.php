<?php
error_reporting('E_ALL && ~E_NOTICE');
ini_set('display_errors','Off');

defined('BASEPATH') OR exit('No direct script access allowed');

class Results extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    /*http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/getuserresult/?format=json*/
    public function index()
    {
        $suma=0;
        $answered=$this->input->post('answered', TRUE);
        $quizid=$this->input->post('quizid', TRUE);
        $serverurl=$this->input->post('serverurl', TRUE);
        $compartir=$this->input->post('compartir', TRUE);
        
        $soyComo=$this->input->post('soyComo', TRUE);
        $FBdescriptionShare=$this->input->post('FBdescriptionShare', TRUE);
        $TWITTERtextShare=$this->input->post('TWITTERtextShare', TRUE);

        $vecans=explode(",",$answered);
        //Refactor resultado de quizzes
        $rank_result=array();
        for($i=0;$i<count($vecans);$i++):
            $vec=explode("-",$vecans[$i]);
            if(strlen($vec[1])>4)exit("Forbidden access");
            $this->db->select('result_id');
            $this->db->from('answer');
            $this->db->where('id', $vec[1]);
            $query=$this->db->get()->result();
            if(!isset($rank_result["'".$query[0]->result_id."'"]))$rank_result["'".$query[0]->result_id."'"]=0;
            $rank_result["'".$query[0]->result_id."'"]=$rank_result["'".$query[0]->result_id."'"]+1;

        endfor;

        arsort($rank_result);
        //print_r($rank_result);
        //exit(key($rank_result));

        if(strlen(key($rank_result))>4)exit("Forbidden access");
        if(strlen($quizid)>4)exit("Forbidden access");
        $this->db->select('texto, result, result.imageurl, result.channel_id, quiz.imageurlshort');
        $this->db->from('quiz_result');
        $this->db->join('result', 'result.id = quiz_result.result_id');
        $this->db->join('quiz', 'quiz.id = quiz_result.quiz_id');
        $this->db->where("result.id=".key($rank_result)." AND quiz_id=$quizid");
        $this->db->limit('1');
        $query = $this->db->get()->result();
        $query["resultado"]=$query;

        $sqlstr="UPDATE result SET votos=votos+1 WHERE id=".key($rank_result);
        $this->db->query($sqlstr);
        /*End refactor */

        $query["serverurl"]=$serverurl;
        $query["compartir"]=$compartir;

        $query["soyComo"]=$soyComo;
        $query["FBdescriptionShare"]=$FBdescriptionShare;
        $query["TWITTERtextShare"]=$TWITTERtextShare;

		$query["botVolver"]=$botVolver;
		$query["hasCompletado"]=$hasCompletado;

        //$query["debugThis"]=$sqlstr;
        //$query["debugThis"]="percentvara:$percentvara priority:$priority priorityMaxOpc:$priorityMaxOpc priorityMaxRes:$priorityMaxRes prioritySelection:$prioritySelection suma:$suma priorities:$priorities vecans:".implode($vecans);
        $query["debugThis"]="";
        $this->load->view('result',$query);
    }
}
