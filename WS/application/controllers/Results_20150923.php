<?php
error_reporting('E_ALL && ~E_NOTICE');
ini_set('display_errors','Off');

defined('BASEPATH') OR exit('No direct script access allowed');

class Results extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    /*http://abzdev.no-ip.info/mozan/quizzes/index.php/quiz/getuserresult/?format=json*/
    public function index()
    {
        $suma=0;
        $answered=$this->input->post('answered', TRUE);
        $quizid=$this->input->post('quizid', TRUE);
        $serverurl=$this->input->post('serverurl', TRUE);
        $compartir=$this->input->post('compartir', TRUE);

        $vecans=explode(",",$answered);
        for($i=0;$i<count($vecans);$i++):
           $vec=explode("-",$vecans[$i]);
           $this->db->select('priority');
           $this->db->from('question_answer');
           $this->db->where('question_id', $vec[0]);
           $this->db->where('answer_id', $vec[1]);
           $query = $this->db->get()->result();
           $suma+=$query[0]->priority;
        endfor;
        $priority=round($suma/$i); //No se su porcentaje! (*)

        //Prioridad de opciones que tiene el 100% de ese quiz
        $query = $this->db->query("SELECT QA.priority FROM question_answer QA, quiz_question QQ WHERE QA.question_id=QQ.question_id AND QQ.quiz_id=$quizid ORDER BY QA.priority DESC LIMIT 1");
        $row = $query->row();
        $priorityMax=$row->priority;

        //Saco en que porcentaje esta la prioridad en las opciones (*)
        $percentvara=($priority*100)/$priorityMax;

        //Prioridad de resultados que tiene el 100% de ese quiz

        $query = $this->db->query("SELECT priority FROM quiz_result WHERE quiz_id=$quizid ORDER BY priority DESC LIMIT 1");
        $row = $query->row();
        $priorityMax=$row->priority;

        $priority=round(($percentvara*$priorityMax)/100);

        //Busco esa prioridad en los resultado de ese Quiz
        $this->db->select('texto, result, result.imageurl, quiz.imageurlshort');
        $this->db->from('quiz_result');
        $this->db->join('result', 'result.id = quiz_result.result_id');
        $this->db->join('quiz', 'quiz.id = quiz_result.quiz_id');
        $this->db->where("priority=$priority AND quiz_id=$quizid");
        $query = $this->db->get()->result();
        $query["resultado"]=$query;

        $query["serverurl"]=$serverurl;
        $query["compartir"]=$compartir;

        $this->load->view('result',$query);
    }
}
