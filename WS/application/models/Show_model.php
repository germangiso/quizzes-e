<?php
class Show_model extends CI_Model {

        public $name;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

       function form_insert($data){
            $this->db->insert('channel', $data);
            $this->session->set_userdata('channelid', $this->db->insert_id());
	        redirect('/backend/quizzes_management', 'refresh');
        }

}
