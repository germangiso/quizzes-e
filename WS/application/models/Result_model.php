<?php
class Result_model extends CI_Model {

        public $result;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

       public function get_result_names()
        {

            $query = $this->db->select('id, texto')
                ->where('channel_id', $this->session->userdata("channelid"))
                ->order_by('id', 'desc')
                ->get('result');

                //$query = $this->db->get('Result');
                return $query->result();
        }

}
