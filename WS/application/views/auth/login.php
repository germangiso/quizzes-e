

<link type="text/css" rel="stylesheet" href="../../assets/grocery_crud/css/bootstrap.css" />

<style>
.container{
	 background: gainsboro;
  padding: 25px;
  margin-top: 25px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  max-width: 450px;
}
.logos{
	margin-right: auto;
  margin-left: auto;
	 max-width: 450px;
	 text-align: center;
}
.logos img{
	margin: 10px;
}
</style>

<div class="container">

	<h1><?php echo lang('login_heading');?></h1>
	<p><?php echo lang('login_subheading');?></p>
	<hr />
	<div id="infoMessage"><?php echo $message;?></div>

	<?php echo form_open("auth/login");?>

	  <p>
		<?php echo lang('login_identity_label', 'identity');?>
		<?php echo form_input($identity);?>
	  </p>

	  <p>
		<?php echo lang('login_password_label', 'password');?>
		<?php echo form_input($password);?>
	  </p>

	  <p>Show: <?php
					$options[''] = 'Seleccione un Show';
					$options = array();
						$options[0] = 'Nuevo Show';
					foreach($channel as $option)
					{
						$options[$option->id] = $option->name;
					}
					echo form_dropdown('channel', $options, '');
					?>
	  </p>

	  <p>
		<?php echo lang('login_remember_label', 'remember');?>
		<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
	  </p>


	  <p><?php echo form_submit('submit', lang('login_submit_btn'));?></p>

	<?php echo form_close();?>

	<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>

</div>

<br clear="all" />

<div class="logos">
E! Entertainment Television (c) <?php echo(date('Y'));?>
</div>
