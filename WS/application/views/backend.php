<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<script>
function redirect(url, channelid)
{
    location.href=url+"/"+channelid;
}
</script>
<link type="text/css" rel="stylesheet" href="../../assets/grocery_crud/css/bootstrap.css" />
</head>
<body>

	<div class="top">
        <a href='<?php echo site_url('backend/result_management')?>' class='color-5'>1) Resultados</a>
		<a href='<?php echo site_url('backend/answers_management')?>' class='color-1'>2) Opciones</a>
		<a href='<?php echo site_url('backend/questions_management')?>' class='color-2'>3) Preguntas</a>
		<a href='<?php echo site_url('backend/quizzes_management')?>' class='color-3'>4) Quizzes</a>
		<a href='<?php echo site_url('backend/channel_management')?>' class='color-4'>Shows</a>
        &nbsp;
		<a style="float:right;" href='<?php echo site_url('auth/logout')?>'>Salir de sistema</a>
        <div style="float:right;color:#ffffff;">Selección de Show:
                <?php 
                $redirUrl=site_url('backend/setChannel');
                echo form_dropdown('channel', $channels, $channelidSelected,"onChange=redirect('".$redirUrl."',this.value)");
                ?></div>
		
	</div>
	<div style='height:20px;'></div>  

<div class="container">

    <div class="intro">
          <p><strong>Paso 1:</strong> Sube todos los resultados posibles que un usuario puede conseguir.<br /><strong>Paso 2</strong>: Subir opciones de una pregunta. <br /><strong>Paso 3:</strong> Subir pregunta/s y asociar las opciones cargadas en paso 2. <br /><strong>Paso 4:</strong> Subir quiz, asociarle los resultados posibles del paso 1 y la/s preguntas subidas en paso 3.</p>
    </div>

    <div style='height:20px;'></div>
    <div>
		<?php echo $output; ?>
    </div>

</div>

<?php switch($section){

case "answers":
    $color1="rgb(102, 47, 47);";
    $color2="#FF7E77";
    $color3="#F0948F";
    $color4="#FFC8C5";
    $color5="#F9DADA";
    break;
case "questions":
    $color1="rgb(95, 74, 26);";
    $color2="#CDAB5D";
    $color3="#DAC085";
    $color4="#E4D7BA";
    $color5="rgb(245, 230, 195)";
    break;   
case "quizzes":
    $color1="#2F491A";
    $color2="#97B281";
    $color3="#B6CFA3";
    $color4="#D4FFB2";
    $color5="#D3E5C3";
    break; 
case "channels":
    $color1="#003CB8";
    $color2="#B5C7EC";
    $color3="#7BA2F2";
    $color4="#5B8CF2";
    $color5="#D6DCEA";
    break;
case "resultados":
    $color1="#411A57";
    $color2="#AC76CB";
    $color3="#BD86DD";
    $color4="#C199D8";
    $color5="#D8A8F4";
    break;
default:
    $color1="rgb(132, 83, 83);";
    $color2="#FF7E77";
    $color3="#F0948F";
    $color4="#FFC8C5";
    $color5="#F9DADA";
    break;
}
?>
<style>
body{
    color:<?php echo($color1);?>;
	margin: 0;
}
.container{
  width: 90%;
  margin-left: auto;
  margin-right: auto;
}
.intro{
	padding: 15px;
	background: gainsboro;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
}

table.dataTable tr.odd td.sorting_1{
    background:<?php echo($color2);?>;
}

table.dataTable tr.odd {
    background:<?php echo($color3);?>;
}

table.dataTable tr.even td.sorting_1{
    background:<?php echo($color4);?>;
}

.form-field-box.even{
    background:<?php echo($color5);?>;
}
</style>

</body>
</html>
