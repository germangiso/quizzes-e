<link type="text/css" rel="stylesheet" href="../../assets/grocery_crud/css/bootstrap.css" />

<style>
.container{
	 background: gainsboro;
  padding: 25px;
  margin-top: 25px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  max-width: 450px;
}
.logos{
	margin-right: auto;
  margin-left: auto;
	 max-width: 450px;
	 text-align: center;
}
.logos img{
	margin: 10px;
}
</style>


<div class="container">

	<h1>Nuevo Show</h1>
	<p>
Para comenzar una nueva trivia debes ingresar un nuevo show, de lo contrario no puedes acceder a ninguna sección del administrador.<br />
Puedes <a href="<?php echo site_url('auth/logout'); ?>">volver a ingresar</a> al administrador seleccionando un show existente desde el login.</p>
	<hr />
	<div id="infoMessage"></div>

<?php echo form_open('', '');
$data = array(
              'name'        => 'name',
              'id'          => 'name',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:100%',
            );?>
	  <p>
		<?php echo ("Nombre del show:".form_input($data));?><?php echo form_error('name');?>
	  </p>

	  <p><?php echo form_close();?></p>

	</form>

</div>


