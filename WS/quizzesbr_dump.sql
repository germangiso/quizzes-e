-- MySQL dump 10.14  Distrib 5.5.32-MariaDB, for Linux (x86_64)
--
-- Host: preprodabzdb    Database: quizzesbr
-- ------------------------------------------------------
-- Server version	5.5.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` int(11) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `result_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (1,'The Rolling Stones','2015-05-28 19:07:40',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/1a.jpg',NULL),(2,'One Direction','2015-05-28 19:07:57',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/1b.jpg',NULL),(3,'The Beatles','2015-05-28 19:08:12',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/1c.jpg',NULL),(4,'Coldplay','2015-05-28 19:08:25',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/1d.jpg',NULL),(5,'U2','2015-05-28 19:09:05',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/1e.jpg',NULL),(6,'Queen','2015-05-28 19:09:17',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/1f.jpg',NULL),(7,'Palácio de Buckingham','2015-06-11 20:40:00',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/2a.jpg',NULL),(8,'Palácio de Kensington','2015-06-11 20:40:11',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/2b.jpg',NULL),(9,'Castillo de Windsor','2015-05-28 19:12:32',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/2c.jpg',NULL),(10,'Castillo y finca de Balmoral','2015-05-28 19:12:46',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/2d.jpg',NULL),(11,'Si','2015-04-08 19:48:22',1,'http://wp.patheos.com.s3.amazonaws.com/blogs/poptheology/files/2013/10/Si.jpg',NULL),(12,'Jase','2015-04-08 19:49:19',1,'http://img.poptower.com/pic-85141/jase-robertson-duck-dynasty.jpg?d=600',NULL),(13,'Lacio','2015-04-15 17:24:48',1,NULL,NULL),(14,'Rulos','2015-04-15 17:25:02',1,NULL,NULL),(15,'Dreadlocks','2015-04-15 17:25:10',1,NULL,NULL),(16,'Pelado','2015-04-15 17:25:16',1,NULL,NULL),(17,'Mansión y finca de Sandringham','2015-05-28 19:13:00',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/2e.jpg',NULL),(18,'Palacio de Holyroodhouse','2015-05-28 19:13:39',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/2f.jpg',NULL),(19,'¡Me encantan los escándalos! Sobre todo si hablan de mí. ','2015-05-28 19:15:49',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/3a.jpg',NULL),(20,'¿Oscuro?, ¿escándalo? … ¿Qué?','2015-05-28 19:16:13',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/3b.jpg',NULL),(21,'Es mi familia y la apoyaré en todo momento. ','2015-05-28 19:16:32',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/3c.jpg',NULL),(22,'Procuro mantenerme al margen de la situación.','2015-05-28 19:16:51',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/3d.jpg',NULL),(23,'Mantener la calma y la discreción. Ya formularás un plan para mantener tu imagen perfecta.','2015-05-28 19:17:08',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/3e.jpg',NULL),(24,'Aprovechar la oportunidad para enemistar a algunas personas.','2015-05-28 19:17:20',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/3f.jpg',NULL),(25,'Celular con cámara moderna –Mi Samsung Galaxy A5 para poder capturar los mejores y más jugosos momentos de la experiencia.','2015-05-28 19:27:04',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/4a.jpg',NULL),(26,'Mascota – A veces son molestos, ¡pero es imposible vivir sin mi perro!','2015-05-28 19:27:40',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/4b.jpg',NULL),(27,'Maquillaje –Lejos de casa, ¡pero siempre regia!','2015-05-28 19:28:14',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/4c.jpg',NULL),(28,'Carterita (Botella) - Sólo hay una manera de sobrevivir largas travesías','2015-05-28 19:29:06',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/4d.jpg',NULL),(29,'Arma de defensa personal –Hey!, nunca se está demasiado preparado','2015-05-28 19:30:25',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/4e.jpg',NULL),(30,'Micrófono oculto – Nunca se sabe cuándo escucharás algo digno de ser grabado','2015-05-28 19:30:45',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/4f.jpg',NULL),(31,'Pippa Middleton','2015-05-28 19:33:35',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/5a.jpg',NULL),(32,'Eugenia y Beatriz de York','2015-05-28 19:33:53',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/5b.jpg',NULL),(33,'Diana de Gales','2015-05-28 19:34:07',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/5c.jpg',NULL),(34,'Carlota Casiraghi','2015-05-28 19:34:31',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/5d.jpg',NULL),(35,'Charlène de Mónaco','2015-05-28 19:40:12',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/5e.jpg',NULL),(36,'Letizia Ortiz','2015-05-28 19:40:35',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/5f.jpg',NULL),(37,'Una tiara','2015-05-28 20:00:13',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/6a.jpg',NULL),(38,'Sombrero Fascinator','2015-05-28 20:01:01',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/6b.jpg',NULL),(39,'Una banda real','2015-05-28 20:01:23',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/6c.jpg',NULL),(40,'Bufanda con tu escudo familiar','2015-05-28 20:01:46',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/6d.jpg',NULL),(41,'Una corona imponente','2015-05-28 20:02:08',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/6e.jpg',NULL),(42,'Un cetro','2015-05-28 20:03:10',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/6f.jpg',NULL),(43,'Festejar hasta perder la conciencia','2015-05-28 20:04:44',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/7a.jpg',NULL),(44,'Disfrutar en un lugar exclusivo, donde no haya gente común… Ugh!','2015-05-28 20:04:58',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/7b.jpg',NULL),(45,'Ir de cacería y tomar aire fresco','2015-05-28 20:05:16',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/7c.jpg',NULL),(46,'Relajarte con un libro en un café','2015-05-28 20:05:32',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/7d.jpg',NULL),(47,'¿Tiempo libre? No sé qué es eso.','2015-05-28 20:05:55',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/7e.jpg',NULL),(48,'Buscar aventuras románticas','2015-05-28 20:06:11',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/7f.jpg',NULL),(49,'Valija 1','2015-05-28 20:08:45',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/8a.jpg',NULL),(50,'Valija 2','2015-05-28 20:08:55',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/8b.jpg',NULL),(51,'Valija 3','2015-05-28 20:10:27',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/8c.jpg',NULL),(52,'Valija 4','2015-05-28 20:11:29',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/8d.jpg',NULL),(53,'Valija 5','2015-05-28 20:11:50',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/8e.jpg',NULL),(54,'Valija 6','2015-05-28 20:12:00',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/8f.jpg',NULL);
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
INSERT INTO `channel` VALUES (2,'THE ROYALS');
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Administrator'),(2,'members','General User');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keys`
--

LOCK TABLES `keys` WRITE;
/*!40000 ALTER TABLE `keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'Qual é sua banda favorita?','2015-06-11 20:44:47',2),(2,'Em qual desses lugares você gostaria de viver?','2015-06-11 20:45:02',2),(4,'Se você descobrisse um escândalo que envolve sua família, qual seria sua reação?','2015-06-11 20:45:27',2),(5,'Que personaje te gusta más de Duck Dynasty?','2015-04-08 19:44:42',1),(6,'Que tipo de pelo tiene Mary?','2015-04-15 17:26:27',1),(7,'Em uma viagem longa, o que é indispensável para você?','2015-06-11 20:45:59',2),(8,'Para mulheres: Qual dessas figuras reais tem o estilo parecido com o seu? Para homens: com qual delas voc&ecirc; sairia?','2015-06-12 15:44:11',2),(9,'Qual desses acessórios reais é seu preferido?','2015-06-11 20:46:40',2),(10,'O que você gosta de fazer no seu tempo livre?','2015-06-11 20:46:55',2),(11,'Se voc&ecirc; tivesse que viajar de f&eacute;rias, qual mala escolheria?','2015-06-12 15:45:43',2);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_answer`
--

DROP TABLE IF EXISTS `question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` varchar(45) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_answer`
--

LOCK TABLES `question_answer` WRITE;
/*!40000 ALTER TABLE `question_answer` DISABLE KEYS */;
INSERT INTO `question_answer` VALUES (1,'1',2,'0000-00-00 00:00:00',5),(2,'1',1,'0000-00-00 00:00:00',2),(35,'2',7,'2015-05-28 19:14:11',0),(34,'2',10,'2015-05-28 19:14:11',3),(5,'3',4,NULL,0),(41,'4',19,'2015-05-28 19:18:16',4),(40,'4',22,'2015-05-28 19:18:16',2),(39,'4',20,'2015-05-28 19:18:16',3),(38,'4',23,'2015-05-28 19:18:16',0),(10,'5',12,NULL,0),(11,'5',9,NULL,1),(12,'5',11,NULL,2),(13,'5',10,NULL,3),(14,'6',13,NULL,0),(15,'6',16,NULL,1),(16,'6',15,NULL,2),(17,'6',14,NULL,3),(37,'4',21,'2015-05-28 19:18:16',1),(36,'4',24,'2015-05-28 19:18:16',5),(20,'2',18,'2015-05-11 20:07:53',5),(21,'2',17,'2015-05-11 20:07:53',4),(33,'2',8,'2015-05-28 19:14:11',1),(32,'2',9,'2015-05-28 19:14:11',2),(31,'1',6,'2015-05-28 19:10:19',3),(30,'1',3,'2015-05-28 19:10:19',1),(29,'1',4,'2015-05-28 19:10:19',4),(28,'1',5,'2015-05-28 19:10:19',0),(42,'7',25,'2015-05-28 19:31:53',1),(43,'7',26,'2015-05-28 19:31:53',3),(44,'7',27,'2015-05-28 19:31:53',0),(45,'7',30,'2015-05-28 19:31:53',5),(46,'7',29,'2015-05-28 19:31:53',4),(47,'7',28,'2015-05-28 19:31:53',2),(48,'8',33,'2015-05-28 19:42:11',1),(49,'8',35,'2015-05-28 19:42:11',5),(50,'8',36,'2015-05-28 19:42:11',0),(51,'8',32,'2015-05-28 19:42:11',4),(52,'8',31,'2015-05-28 19:42:11',3),(53,'8',34,'2015-05-28 19:42:11',2),(54,'9',42,'2015-05-28 20:04:16',3),(55,'9',41,'2015-05-28 20:04:16',0),(56,'9',37,'2015-05-28 20:04:16',2),(57,'9',39,'2015-05-28 20:04:16',1),(58,'9',40,'2015-05-28 20:04:16',5),(59,'9',38,'2015-05-28 20:04:16',4),(60,'10',43,'2015-05-28 20:07:14',2),(61,'10',44,'2015-05-28 20:07:14',4),(62,'10',45,'2015-05-28 20:07:14',1),(63,'10',48,'2015-05-28 20:07:14',5),(64,'10',46,'2015-05-28 20:07:14',3),(65,'10',47,'2015-05-28 20:07:14',0),(66,'11',49,'2015-05-28 20:43:58',0),(67,'11',50,'2015-05-28 20:43:58',1),(68,'11',54,'2015-05-28 20:43:58',2),(69,'11',53,'2015-05-28 20:43:58',3),(70,'11',51,'2015-05-28 20:43:58',4),(71,'11',52,'2015-05-28 20:43:58',5);
/*!40000 ALTER TABLE `question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz`
--

DROP TABLE IF EXISTS `quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` int(11) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `imageurlshort` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz`
--

LOCK TABLES `quiz` WRITE;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
INSERT INTO `quiz` VALUES (1,'Quiz',1,'2015-05-29 19:21:42',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/theroyals_bg.jpg','http://br.eonline.com/experience/quizzes/assets/img/theroyals/theroyals_mobile.jpg'),(2,'Quién da más Texas',1,'2015-04-08 19:18:29',1,'http://tmsimg.com/showcards/h3/AllPhotos/8666432/p8666432_b_h3_ab.jpg',NULL),(3,'Duck Dinasty',1,'2015-04-08 19:43:18',1,'http://www.adweek.com/files/imagecache/node-detail/news_article/duck-dynasty-hed-2013_0.jpg',NULL);
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz_question`
--

DROP TABLE IF EXISTS `quiz_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz_question`
--

LOCK TABLES `quiz_question` WRITE;
/*!40000 ALTER TABLE `quiz_question` DISABLE KEYS */;
INSERT INTO `quiz_question` VALUES (8,1,2,NULL,0),(20,1,4,'2015-05-29 20:53:51',7),(3,2,4,NULL,0),(5,3,5,NULL,0),(6,2,6,NULL,1),(19,1,7,'2015-05-29 20:53:51',6),(18,1,1,'2015-05-29 20:53:51',5),(17,1,9,'2015-05-29 20:53:51',4),(16,1,10,'2015-05-29 20:53:51',3),(15,1,11,'2015-05-29 20:53:51',2),(14,1,8,'2015-05-28 19:42:52',1);
/*!40000 ALTER TABLE `quiz_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz_result`
--

DROP TABLE IF EXISTS `quiz_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `result_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz_result`
--

LOCK TABLES `quiz_result` WRITE;
/*!40000 ALTER TABLE `quiz_result` DISABLE KEYS */;
INSERT INTO `quiz_result` VALUES (1,1,1,0),(2,1,2,1),(3,1,6,4),(4,1,5,3),(5,1,4,2),(6,1,7,5),(7,1,8,6),(8,1,9,7);
/*!40000 ALTER TABLE `quiz_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `result` text NOT NULL,
  `channel_id` int(11) NOT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `texto` text NOT NULL,
  `votos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (1,'a Rainha Helena:',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/result_1.jpg','Você é a expressão do controle e faz tudo calculado, mas não se engane! Por trás dessa imagem perfeita há uma pessoa que sabe se divertir.',0),(2,'o Rei Simon:',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/result_2.jpg','Para você, família é o mais importante e você está sempre disposto a sacrificar tudo para o bem estar deles. É real, nobre e sabe seguir seus instintos.',0),(4,'o Duque de York Cyrus\r\n',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/result_3.jpg','É a representação física da maldade. Um egoísta cruel, que faz de tudo para conseguir o que quer. ',0),(5,'Ophelia\r\n',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/result_4.jpg','Vive sem pretensão, gosta de coisas simples e sabe encontrar a felicidade nas pequenas coisas da vida.',0),(6,'as Princesas Penélope e Maribel:',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/result_5.jpg','Vive com a cabeça em outro planeta. Quase nunca sabe o que acontece ao seu redor. A parte boa? Ninguém consegue ficar do seu lado sem rir com você… Ou de você.',0),(7,'o Príncipe Liam',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/result_6.jpg','Arremata corações por onde passa e apesar de ter sentimentos sinceros, às vezes, a lúxuria faz com que ele seja mal julgado. ',0),(8,'a Princesa Eleanor:',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/result_7.jpg','Tem dinheiro, tem poder e sabe usar isso da melhor forma… Pelo menos é no que acredita! Sua alma festeira e rebeldia sem causa podem trazer mais problemas do que imagina.',0),(9,'Gemma',2,'http://br.eonline.com/experience/quizzes/assets/img/theroyals/result_8.jpg','A manipulação é sua especialidade e é capaz de qualquer coisa para conseguir o que quer: se casar.',0);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1','administrator','$2y$08$ewzn2ZHfZWLrtQqB9d2udekYEf5UMztAMXueSGxiJb3xdUyx6dnX.','','admin@admin.com','',NULL,NULL,NULL,1268889823,1443018187,1,'Admin','istrator','ADMIN','0'),(2,'190.111.232.51','maximiliano ozan','$2a$08$/6GOMzF1zz0fvngDytxDKeFnlEvqtA3eBWTbYHHUZ5/uk.jYiyEYu',NULL,'mozan@abzcomunicacion.com',NULL,NULL,NULL,'jkhdbjxBoXLpX9fAxv0n5e',1428333679,1434397167,1,'maximiliano','ozan','ABZ','5252.0450');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (7,1,2),(6,1,1),(4,2,1),(5,2,2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-31 10:59:09
