-- MySQL dump 10.14  Distrib 5.5.32-MariaDB, for Linux (x86_64)
--
-- Host: preprodabzdb    Database: quizzes
-- ------------------------------------------------------
-- Server version	5.5.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` int(11) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `result_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (1,'The Rolling Stones','2015-09-23 14:13:51',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/1a.jpg',6),(2,'One Direction','2015-09-23 14:18:11',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/1b.jpg',4),(3,'The Beatles','2015-09-23 14:18:21',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/1c.jpg',1),(4,'Coldplay','2015-09-23 14:18:56',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/1d.jpg',7),(5,'U2','2015-09-23 15:59:33',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/1e.jpg',5),(6,'Queen','2015-09-23 15:59:46',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/1f.jpg',2),(7,'Palacio de Buckingham','2015-09-23 16:00:05',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/2a.jpg',1),(8,'Palacio de Kensington','2015-09-23 16:00:13',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/2b.jpg',2),(9,'Castillo de Windsor','2015-09-23 16:00:24',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/2c.jpg',4),(10,'Castillo y finca de Balmoral','2015-09-23 16:00:34',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/2d.jpg',5),(11,'Si','2015-04-08 19:48:22',1,'http://wp.patheos.com.s3.amazonaws.com/blogs/poptheology/files/2013/10/Si.jpg',NULL),(12,'Jase','2015-04-08 19:49:19',1,'http://img.poptower.com/pic-85141/jase-robertson-duck-dynasty.jpg?d=600',NULL),(13,'Lacio','2015-04-15 17:24:48',1,NULL,NULL),(14,'Rulos','2015-04-15 17:25:02',1,NULL,NULL),(15,'Dreadlocks','2015-04-15 17:25:10',1,NULL,NULL),(16,'Pelado','2015-04-15 17:25:16',1,NULL,NULL),(17,'Mansión y finca de Sandringham','2015-09-23 16:00:45',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/2e.jpg',6),(18,'Palacio de Holyroodhouse','2015-09-23 16:01:04',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/2f.jpg',9),(19,'¡Me encantan los escándalos! Sobre todo si hablan de mí. ','2015-09-23 16:01:52',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/3a.jpg',9),(20,'¿Oscuro?, ¿escándalo? … ¿Qué?','2015-09-23 16:02:05',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/3b.jpg',4),(21,'Es mi familia y la apoyaré en todo momento. ','2015-09-23 16:02:16',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/3c.jpg',2),(22,'Procuro mantenerme al margen de la situación.','2015-09-23 16:02:32',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/3d.jpg',5),(23,'Mantener la calma y la discreción. Ya formularás un plan para mantener tu imagen perfecta.','2015-09-23 16:02:58',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/3e.jpg',5),(24,'Aprovechar la oportunidad para enemistar a algunas personas.','2015-09-23 16:03:24',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/3f.jpg',7),(25,'Celular con cámara moderna –Mi Samsung Galaxy A5 para poder capturar los mejores y más jugosos momentos de la experiencia.','2015-09-23 16:04:32',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/4a.jpg',1),(26,'Mascota – A veces son molestos, ¡pero es imposible vivir sin mi perro!','2015-09-23 16:04:11',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/4b.jpg',8),(27,'Maquillaje –Lejos de casa, ¡pero siempre regia!','2015-09-23 16:04:50',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/4c.jpg',6),(28,'Carterita (Botella) - Sólo hay una manera de sobrevivir largas travesías','2015-09-23 16:05:17',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/4d.jpg',2),(29,'Arma de defensa personal –Hey!, nunca se está demasiado preparado','2015-09-23 16:05:38',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/4e.jpg',4),(30,'Micrófono oculto – Nunca se sabe cuándo escucharás algo digno de ser grabado','2015-09-23 16:06:10',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/4f.jpg',9),(31,'Pippa Middleton','2015-09-23 16:06:27',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/5a.jpg',6),(32,'Eugenia y Beatriz de York','2015-09-23 16:06:37',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/5b.jpg',9),(33,'Diana de Gales','2015-09-23 16:06:48',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/5c.jpg',2),(34,'Carlota Casiraghi','2015-09-23 16:07:01',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/5d.jpg',1),(35,'Charlène de Mónaco','2015-09-23 16:07:14',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/5e.jpg',8),(36,'Letizia Ortiz','2015-09-23 16:07:27',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/5f.jpg',7),(37,'Una tiara','2015-09-23 16:07:41',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/6a.jpg',1),(38,'Sombrero Fascinator','2015-09-23 16:07:55',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/6b.jpg',2),(39,'Una banda real','2015-09-23 16:08:08',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/6c.jpg',4),(40,'Bufanda con tu escudo familiar','2015-09-23 16:08:23',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/6d.jpg',5),(41,'Una corona imponente','2015-09-23 16:08:46',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/6e.jpg',6),(42,'Un cetro','2015-09-23 16:08:58',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/6f.jpg',7),(43,'Festejar hasta perder la conciencia','2015-09-23 16:09:17',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/7a.jpg',6),(44,'Disfrutar en un lugar exclusivo, donde no haya gente común… Ugh!','2015-09-23 16:09:33',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/7b.jpg',8),(45,'Ir de cacería y tomar aire fresco','2015-09-23 16:09:52',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/7c.jpg',1),(46,'Relajarte con un libro en un café','2015-09-23 16:10:04',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/7d.jpg',5),(47,'¿Tiempo libre? No sé qué es eso.','2015-09-23 16:10:41',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/7e.jpg',9),(48,'Buscar aventuras románticas','2015-09-23 16:10:55',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/7f.jpg',7),(49,'Valija 1','2015-09-23 16:11:13',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/8a.jpg',5),(50,'Valija 2','2015-09-23 16:11:24',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/8b.jpg',8),(51,'Valija 3','2015-09-23 16:11:44',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/8c.jpg',4),(52,'Valija 4','2015-09-23 16:11:55',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/8d.jpg',6),(53,'Valija 5','2015-09-23 16:12:13',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/8e.jpg',1),(54,'Valija 6','2015-09-23 16:12:33',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/8f.jpg',9);
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
INSERT INTO `channel` VALUES (2,'THE ROYALS');
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Administrator'),(2,'members','General User');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keys`
--

LOCK TABLES `keys` WRITE;
/*!40000 ALTER TABLE `keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'¿Cúal es tu banda favorita?','2015-05-28 19:10:53',2),(2,'¿En cuál de estas residencies reales te gustaría vivir? ','2015-05-28 19:11:02',2),(4,'Si se descubre un oscuro escándalo que involucre a tu familia, ¿Cuál sería tu reacción? ','2015-06-05 12:54:32',2),(5,'Que personaje te gusta más de Duck Dynasty?','2015-04-08 19:44:42',1),(6,'Que tipo de pelo tiene Mary?','2015-04-15 17:26:27',1),(7,'En un viaje largo, ¿cuál de estos elementos es imprescindible para ti? ','2015-06-05 14:01:05',2),(8,'¿Cómo cual de estas chicas de la realeza te vestirías? Si eres chico, ¿Con quién saldrías?','2015-06-05 12:53:23',2),(9,'¿Cuál de estos accesorios reales sería tu preferido? ','2015-05-28 18:54:06',2),(10,'¿Qué prefieres hacer con tu tiempo libre?','2015-06-05 12:41:01',2),(11,'¿Si tuvieras que viajar a otro país en un viaje de placer, cuál de estas valijas elegirías?','2015-05-28 19:06:48',2);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_answer`
--

DROP TABLE IF EXISTS `question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` varchar(45) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_answer`
--

LOCK TABLES `question_answer` WRITE;
/*!40000 ALTER TABLE `question_answer` DISABLE KEYS */;
INSERT INTO `question_answer` VALUES (1,'1',2,'0000-00-00 00:00:00',5),(2,'1',1,'0000-00-00 00:00:00',2),(35,'2',7,'2015-05-28 19:14:11',0),(34,'2',10,'2015-05-28 19:14:11',3),(5,'3',4,NULL,0),(41,'4',19,'2015-05-28 19:18:16',4),(40,'4',22,'2015-05-28 19:18:16',2),(39,'4',20,'2015-05-28 19:18:16',3),(38,'4',23,'2015-05-28 19:18:16',0),(10,'5',12,NULL,0),(11,'5',9,NULL,1),(12,'5',11,NULL,2),(13,'5',10,NULL,3),(14,'6',13,NULL,0),(15,'6',16,NULL,1),(16,'6',15,NULL,2),(17,'6',14,NULL,3),(37,'4',21,'2015-05-28 19:18:16',1),(36,'4',24,'2015-05-28 19:18:16',5),(20,'2',18,'2015-05-11 20:07:53',5),(21,'2',17,'2015-05-11 20:07:53',4),(33,'2',8,'2015-05-28 19:14:11',1),(32,'2',9,'2015-05-28 19:14:11',2),(31,'1',6,'2015-05-28 19:10:19',3),(30,'1',3,'2015-05-28 19:10:19',1),(29,'1',4,'2015-05-28 19:10:19',4),(28,'1',5,'2015-05-28 19:10:19',0),(42,'7',25,'2015-05-28 19:31:53',1),(43,'7',26,'2015-05-28 19:31:53',3),(44,'7',27,'2015-05-28 19:31:53',0),(45,'7',30,'2015-05-28 19:31:53',5),(46,'7',29,'2015-05-28 19:31:53',4),(47,'7',28,'2015-05-28 19:31:53',2),(48,'8',33,'2015-05-28 19:42:11',1),(49,'8',35,'2015-05-28 19:42:11',5),(50,'8',36,'2015-05-28 19:42:11',0),(51,'8',32,'2015-05-28 19:42:11',4),(52,'8',31,'2015-05-28 19:42:11',3),(53,'8',34,'2015-05-28 19:42:11',2),(54,'9',42,'2015-05-28 20:04:16',3),(55,'9',41,'2015-05-28 20:04:16',0),(56,'9',37,'2015-05-28 20:04:16',2),(57,'9',39,'2015-05-28 20:04:16',1),(58,'9',40,'2015-05-28 20:04:16',5),(59,'9',38,'2015-05-28 20:04:16',4),(60,'10',43,'2015-05-28 20:07:14',2),(61,'10',44,'2015-05-28 20:07:14',4),(62,'10',45,'2015-05-28 20:07:14',1),(63,'10',48,'2015-05-28 20:07:14',5),(64,'10',46,'2015-05-28 20:07:14',3),(65,'10',47,'2015-05-28 20:07:14',0),(66,'11',49,'2015-05-28 20:43:58',0),(67,'11',50,'2015-05-28 20:43:58',1),(68,'11',54,'2015-05-28 20:43:58',2),(69,'11',53,'2015-05-28 20:43:58',3),(70,'11',51,'2015-05-28 20:43:58',4),(71,'11',52,'2015-05-28 20:43:58',5);
/*!40000 ALTER TABLE `question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz`
--

DROP TABLE IF EXISTS `quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel_id` int(11) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `imageurlshort` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz`
--

LOCK TABLES `quiz` WRITE;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
INSERT INTO `quiz` VALUES (1,'Quiz',1,'2015-05-29 19:21:42',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/theroyals_bg.jpg','http://la.eonline.com/experience/quizzes/assets/img/theroyals/theroyals_mobile.jpg'),(2,'Quién da más Texas',1,'2015-04-08 19:18:29',1,'http://tmsimg.com/showcards/h3/AllPhotos/8666432/p8666432_b_h3_ab.jpg',NULL),(3,'Duck Dinasty',1,'2015-04-08 19:43:18',1,'http://www.adweek.com/files/imagecache/node-detail/news_article/duck-dynasty-hed-2013_0.jpg',NULL);
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz_question`
--

DROP TABLE IF EXISTS `quiz_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz_question`
--

LOCK TABLES `quiz_question` WRITE;
/*!40000 ALTER TABLE `quiz_question` DISABLE KEYS */;
INSERT INTO `quiz_question` VALUES (8,1,2,NULL,0),(20,1,4,'2015-05-29 20:53:51',7),(3,2,4,NULL,0),(5,3,5,NULL,0),(6,2,6,NULL,1),(19,1,7,'2015-05-29 20:53:51',6),(18,1,1,'2015-05-29 20:53:51',5),(17,1,9,'2015-05-29 20:53:51',4),(16,1,10,'2015-05-29 20:53:51',3),(15,1,11,'2015-05-29 20:53:51',2),(14,1,8,'2015-05-28 19:42:52',1);
/*!40000 ALTER TABLE `quiz_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz_result`
--

DROP TABLE IF EXISTS `quiz_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `result_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz_result`
--

LOCK TABLES `quiz_result` WRITE;
/*!40000 ALTER TABLE `quiz_result` DISABLE KEYS */;
INSERT INTO `quiz_result` VALUES (1,1,1,0),(2,1,2,1),(3,1,6,4),(4,1,5,3),(5,1,4,2),(6,1,7,5),(7,1,8,6),(8,1,9,7);
/*!40000 ALTER TABLE `quiz_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `result` text NOT NULL,
  `channel_id` int(11) NOT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `texto` text NOT NULL,
  `votos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (1,'la Reina Helena',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/result_1.jpg','Eres la máxima expresion del control y el cálculo. Pero no dejemos que esto nos engañe, detrás de esa imagen perfecta, hay una persona que sabe divertirse.',895),(2,'el Rey Simon',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/result_2.jpg','Para ti, la familia es lo más importante y estás dispuesto a sacrificarlo todo por su bienestar. Eres leal, noble y sabes seguir tus instintos. ',158),(4,'el Duque de York Cyrus\r\n',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/result_3.jpg','Eres la representación física de la maldad; egoísta, cruel y harás lo que sea con tal de lograr tus objetivos. ',238),(5,'Ophelia\r\n',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/result_4.jpg','Vives sin pretenciones, eres de gustos sencillos y sabes encontrar felicidad en los pequeños detalles. ',466),(6,'las Princesas Penélope y Maribel',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/result_5.jpg','Tienes la cabeza en otro planeta. Casi nunca sabes lo que sucede a tu alrededor. ¿La parte buena? No hay forma de que los demás no se rían contigo… ¿o de ti? ',529),(7,'el Príncipe Liam',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/result_6.jpg','Rompes corazones a donde vas. Tus sentimientos suelen ser nobles aunque a veces la lujuría te haga una mala jugada. ',269),(8,'la Princesa Eleanor',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/result_7.jpg','Tienes dinero, tienes poder y sabes cómo hacer que se vea bien… o eso es lo que crees. Aunque seas el alma de la fiesta, ser un rebelde sin causa, podría traerte más problemas de los que imaginas.  ',142),(9,'Gemma',2,'http://la.eonline.com/experience/quizzes/assets/img/theroyals/result_8.jpg','La manipulación es tu arte y eres capaz de utilizar todos tus atributos con tal de obtener lo que deseas, que por lo general, es casarte. ',38);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com','',NULL,NULL,NULL,1268889823,1428333638,1,'Admin','istrator','ADMIN','0'),(2,'190.111.232.51','maximiliano ozan','$2a$08$/6GOMzF1zz0fvngDytxDKeFnlEvqtA3eBWTbYHHUZ5/uk.jYiyEYu',NULL,'mozan@abzcomunicacion.com',NULL,NULL,NULL,'TNri6EDFSrRY/Xfj5HwF.O',1428333679,1443029470,1,'maximiliano','ozan','ABZ','5252.0450');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (1,1,1),(2,1,2),(4,2,1),(5,2,2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-31 10:59:00
